require 'ruby-handlebars'
require 'net/http'
require 'json'
require "down"
require "fileutils"

module Fastlane
  module Actions
    class ActionStartAction < Action

      def self.run(options)
        hbs = Handlebars::Handlebars.new
        puts "Manifest URL: " + ENV['MANIFEST_URL']
        uri = URI(ENV['MANIFEST_URL'] || 'http://localhost:3000/')
        params = { :title => ENV['title'] }
        uri.query = URI.encode_www_form(params)
        json = Net::HTTP.get(uri)
        File.open("manifest.json", "w") {
          |manifest| manifest.write(json)
        }
        data = JSON.parse(json)
        File.open("../lib/app_config.dart.template", "r") {
          |file| template = hbs.compile(file.read())
          File.open("../lib/app_config.dart", "w") {
            |target_file| target_file.write(template.call(data["config"]))
          }
        }

        tempfile = Down.download(data["icon"])
        FileUtils.mv(tempfile.path, "../assets/icon/icon.png")
        data["assets"].each {
          |item| FileUtils.mkdir_p("../assets/" + item["dirname"])
            item["files"].each { |file| tempfile = Down.download(file["url"])
              FileUtils.mv(tempfile.path, "../assets/" + item["dirname"] + "/" + file["filename"])
            }
        }
      end

      def self.is_supported?(platform)
        TRUE
      end
    end
  end
end
